//
//  UIImageView+Networking.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

extension UIImageView {
    func image(from link: String) {
        ImageLoadProcessor.shared.loadImage(from: link, into: self)
    }
}
