//
//  RestaurantListTableViewController.swift
//  Garson
//
//  Created by Владимир Елизаров on 27/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit


class RestaurantListViewController: UITableViewController, RestaurantListViewType {
    
    private struct Constants {
        static let cellReuseID = "RestaurantListTableViewCell"
    }
    
    var presenter: RestaurantListPresenterType? {
        didSet {
            if self.viewIfLoaded != nil {
                self.presenter?.viewIsReady()
            }
        }
    }
    
    var currentState: LayerState = .prepare {
        didSet {
            guard self.currentState != oldValue else { return }
            switch self.currentState {
            case .ready:          self.turnToReadyState()
            case .prepare:        self.turnToPrepareState()
            case .error(let msg): self.turnToErrorState(msg)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.turnToPrepareState()
        self.presenter?.viewIsReady()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.currentState == .ready
            , let presenter = self.presenter else {
            return 0
        }
        return presenter.numberOfItems
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellReuseID, for: indexPath)
        guard let restaurantCell = cell as? RestaurantTableViewCell
            , let presenter = self.presenter else {
            return cell
        }
        
        let restaurant = presenter.item(at: indexPath.row)
        restaurantCell.nameLabel.text = restaurant.title
        restaurantCell.descriptionLabel.text = restaurant.description
            ?? restaurant.footerDescription
        if let link = restaurant.imageLink(width: 75, height: 100) {
            restaurantCell.imgView.image(from: link)
        }        
        return restaurantCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // В задании не было ничего о том, что нужно делать при нажатии на ячейку,
        // по этому просто деселектим и оставяем без внимания
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func layerStateDidChange(to newState: LayerState) {
        self.currentState = newState
    }
    
    private func turnToPrepareState() {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = .black
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        self.tableView.backgroundView = activityIndicator
        self.tableView.separatorStyle = .none
    }
    
    fileprivate func turnToErrorState(_ msg: String) {
        self.tableView.separatorStyle = .none
        let errorView = ErrorView.loadFromNib()
        errorView.messageLabel.text = msg
        errorView.onRetryTap = { [weak self] in
            guard let self = self else { return }
            self.currentState = .prepare
            self.presenter?.viewIsReady()
        }
        self.tableView.backgroundView = errorView
        self.tableView.reloadData()
    }
    
    fileprivate func turnToReadyState() {
        self.tableView.separatorStyle = .singleLine
        self.tableView.backgroundView = nil
        self.tableView.reloadData()
    }
}
