//
//  String+CodingKey.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

// MARK: - С целью упрощения кодовой базы, для обеспечения маппинга
// json нативными средствами Swift, String будет наделен способностью
// быть использован как CodingKey в Codable
extension String: CodingKey {
    
    public var stringValue: String {
        return self
    }
    
    public var intValue: Int? {
        return nil
    }
    
    public init?(intValue: Int) {
        return nil
    }
    
    public init?(stringValue: String) {
        self.init(stringLiteral: stringValue)
    }
}
