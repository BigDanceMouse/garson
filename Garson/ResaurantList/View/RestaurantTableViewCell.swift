//
//  RestaurantListTableViewCell.swift
//  Garson
//
//  Created by Владимир Елизаров on 27/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgView.image = nil
        self.nameLabel.text = nil
        self.descriptionLabel.text = nil
    }

}
