//
//  RestaurantListPresenter.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

class RestaurantListPresenter: RestaurantListPresenterType {
    
    
    // MARK: - INTERNAL -
    
    var numberOfItems: Int {
        return self.items.count
    }
    
    init(interactor: RestaurantListInteractorType) {
        self.interactor = interactor
    }
    
    func viewIsReady() {
        self.updateItems()
    }
    
    func item(at index: Int) -> Restaurant {
        return self.items[index]
    }
    
    func withViewInput(viewInput: RestaurantListViewType) -> RestaurantListPresenter {
        self.updateViewInputIfNeeded(with: viewInput)
        return self
    }

    
    // MARK: - PRIVATE -
    
    private weak var viewInput: RestaurantListViewType?
    
    private let interactor: RestaurantListInteractorType
    
    private var items: [Restaurant] = []
    
    private func updateViewInputIfNeeded(with newViewInput: RestaurantListViewType) {
        guard self.viewInput == nil else {
            assertionFailure("Попытка переопределить ранее установленный viewInput")
            return
        }
        self.viewInput = newViewInput
    }
    
    private func updateItems() {
        self.interactor.loadAll(then: { [weak self] result in
            guard let self = self else { return }
            switch result {
                
            case .success(let newRestaurants):
                self.items = newRestaurants
                self.viewInput?.layerStateDidChange(to: .ready)
                
            case .fail(let error):
                print("\(error)")
                let errState = LayerState.error(message: "Возникла ошибка при загрузке")
                self.viewInput?.layerStateDidChange(to: errState)
            }
        })
    }
}

