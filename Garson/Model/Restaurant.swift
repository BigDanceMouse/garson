//
//  Restaurant.swift
//  Garson
//
//  Created by Владимир Елизаров on 27/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

struct Restaurant {
    
    private struct Constant {
        static let widthPattern = "{w}"
        static let heightPattern = "{h}"
    }
    
    let title: String
    
    let description: String?
    
    // На момент разработки не было ни одного элемента у которого
    // description был бы не nil, по этому для демонстрации работы ячеек и их
    // правильного определения высоты решил в качестве альтернативы добавить
    // footerDescription которое было у всех элементов.
    // Не смотря на то что некоторые футеры содержут HTML теги, которые можно
    // было бы
    let footerDescription: String?
    
    private let imageLinkPattern: String?
    
    init(title: String,
         description: String?,
         footerDescription: String?,
         imageLinkPattern: String?) {
        self.title = title
        self.description = description
        self.footerDescription = footerDescription
        self.imageLinkPattern = imageLinkPattern
    }
    
    func imageLink(width: Int, height: Int) -> String? {
        return self.imageLinkPattern?
            .replacingOccurrences(of: Constant.widthPattern, with: "\(width)")
            .replacingOccurrences(of: Constant.heightPattern, with: "\(height)")
    }
}
