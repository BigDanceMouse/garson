//
//  RestaurantListPresenterType.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

protocol RestaurantListPresenterType {
    var numberOfItems: Int { get }
    
    func viewIsReady()
    func item(at index: Int) -> Restaurant
}
