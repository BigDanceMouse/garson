//
//  SettingsViewController.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

protocol SettingsPresenter: class {
    var itemsCount: Int { get }
    func item(at index: Int) -> (processorTitle: String, isCurrentlyUsing: Bool)
    func didSelectItem(at index: Int)
    func clearCache()
}

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private struct Constants {
        static let cellReuseID = "ImageProcessorCell"
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var clearCacheButton: UIButton!

    
    var presenter: SettingsPresenter? {
        didSet {
            if self.viewIfLoaded != nil {
                self.tableView.reloadData()
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.itemsCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellReuseID, for: indexPath)
        if let itemDescriptor = presenter?.item(at: indexPath.row) {
            cell.textLabel?.text = itemDescriptor.processorTitle
            cell.accessoryType = itemDescriptor.isCurrentlyUsing
                ? .checkmark
                : .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.didSelectItem(at: indexPath.row)
        self.tableView.deselectRow(at: indexPath, animated: false)
        self.tableView.reloadData()
    }
    
    @IBAction func clearCacheDidTouched(_ sender: Any) {
        self.presenter?.clearCache()
    }
}
