//
//  SDWebImageProcessor.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit
import SDWebImage

struct SDWebImageProcessor: ImageProcessorProtocol {
    
    func loadImage(from link: String, into imageView: UIImageView) {
        imageView.sd_cancelCurrentImageLoad()
        let url = URL(string: link)
        imageView.sd_setImage(with: url, completed: nil)
    }
    
    func clearCache() {
        SDImageCache.shared().clearDisk(onCompletion: nil)
    }
}
