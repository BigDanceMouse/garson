//
//  RestaurantListViewType.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

protocol RestaurantListViewType: class {
    
    /// Уведомляет вью о том что весь слой принял новое состояние
    func layerStateDidChange(to newState: LayerState)
}
