//
//  GarsonError.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

enum GarsonError: Error {
    
    // Ответ mapi не содежит данных
    case responseFail
}
