//
//  LoaderFabric.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

enum LoaderType: String, CaseIterable {
    case SDWebImage
    case Kingfisher
    case Nuke
    
    fileprivate var processor: ImageProcessorProtocol {
        switch self {
        case .SDWebImage: return SDWebImageProcessor()
        case .Kingfisher: return KingfisherImageProcessor()
        case .Nuke:       return NukeImageProcessor()
        }
    }
}


class ImageLoadProcessor: ImageProcessorProtocol {
    
    static let shared: ImageLoadProcessor = ImageLoadProcessor()
    
    private struct Constants {
        static let lastUsingLoaderKey = "Garson.lastUsedLoaderKey"
    }
    
    var currentLoader: LoaderType {
        didSet {
            if self.currentLoader != oldValue {
                self.recordNewLoader()
            }
        }
    }
    
    init() {
        if let stringValue = UserDefaults.standard.string(forKey: Constants.lastUsingLoaderKey)
            , let loaderType = LoaderType(rawValue: stringValue) {
            self.currentLoader = loaderType
        } else {
            self.currentLoader = .SDWebImage
        }
    }
    
    func loadImage(from link: String, into imageView: UIImageView) {
        self.currentLoader
            .processor
            .loadImage(from: link, into: imageView)
    }
    
    func clearCache() {
        // В задании не было ничего о том, каким образом должен быть очищен
        // кеш при нажатии на кнопку очистки - должен ли он быть очищен только
        // у текущего процессора или для всех процессоров.
        //
        // Если требуется очищать для всех процессоров то должен быть следующий код:
        //  LoaderType.allCases.forEach {
        //     $0.processor.clearCache()
        //  }
        //Вместо того который ниже
        self.currentLoader
            .processor
            .clearCache()
    }
    
    private func recordNewLoader() {
        let defaults = UserDefaults.standard
        defaults.set(currentLoader.rawValue, forKey: Constants.lastUsingLoaderKey)
        defaults.synchronize()
    }
}
