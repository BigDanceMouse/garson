//
//  Result.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

// Для упрощения реализации и уменьшения зависимостей используется
// собственная версия Result вместо стороннего фреймворка
enum Result<T> {
    case success(T)
    case fail(Error)
    
    init(_ f: () throws -> T) {
        do {
            let val = try f()
            self = .success(val)
        } catch {
            self = .fail(error)
        }
    }
    
    func map<U>(_ transform: (T) -> U) -> Result<U> {
        switch self {
        case .success(let val): return .success(transform(val))
        case .fail(let err):    return .fail(err)
        }
    }
}
