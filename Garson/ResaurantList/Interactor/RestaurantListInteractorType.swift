//
//  RestaurantListInteractorType.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

protocol RestaurantListInteractorType {
    func loadAll(then completion: @escaping (Result<[Restaurant]>) -> Void)
}
