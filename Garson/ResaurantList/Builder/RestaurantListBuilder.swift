//
//  RestaurantListBuilder.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

class RestaurantListBuilder: NSObject {
    
    @IBOutlet weak var viewController: RestaurantListViewController!
    
    override func awakeFromNib() {
        let interactor = RestaurantListInteractor()
        let presenter = RestaurantListPresenter(interactor: interactor)
            .withViewInput(viewInput: self.viewController)
        self.viewController.presenter = presenter
    }
}
