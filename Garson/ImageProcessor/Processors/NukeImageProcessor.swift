//
//  NukeImageProcessor.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit
import Nuke

struct NukeImageProcessor: ImageProcessorProtocol {
    
    func loadImage(from link: String, into imageView: UIImageView) {
        Nuke.cancelRequest(for: imageView)
        if let url = URL(string: link) {
            Nuke.loadImage(with: url, into: imageView)
        }
    }
    
    func clearCache() {
        DataLoader.sharedUrlCache.removeAllCachedResponses()
    }
}
