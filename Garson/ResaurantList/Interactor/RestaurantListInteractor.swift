//
//  RestaurantListInteractor.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation
import Alamofire


/// В тестовом задании для упрощения кода используется захардкоженые значения.
/// В реальном проекте для абстрагирования запросов использовалось Moya или
/// подобные фреймворки
private let baseURL = "https://eda.yandex"
private let hardcodedPath = "/api/v2/catalog?latitude=55.762885&longitude=37.597360"

class RestaurantListInteractor: RestaurantListInteractorType {
    
    func loadAll(then completion: @escaping (Result<[Restaurant]>) -> Void) {
        Alamofire.request(baseURL + hardcodedPath).responseData { response in
            guard let data = response.data else {
                completion(.fail(GarsonError.responseFail))
                return
            }
            let decoder = JSONDecoder()
            let result = Result {
                try decoder.decode(ResponsePage.self, from: data)
            }.map { $0.restaurants }
            completion(result)
        }
    }
    
    
    // Вероятно в полноценном проекте многие данные из ответа mapi нужны.
    // В тестовом проекте нужны минимум данных которые, при этом,
    // находятся очень глубоко в дереве json. По этому для упрощения
    // реализации используем мапинг через String и проскакивая промежуточные
    // модели.
    private struct ResponsePage: Decodable {
        
        let restaurants: [Restaurant]
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: String.self)
            let payload = try container.nestedContainer(keyedBy: String.self, forKey: "payload")
            var founded = try payload.nestedUnkeyedContainer(forKey: "foundPlaces")
            
            var places: [Restaurant] = []
            while !founded.isAtEnd {
                let place = try founded
                    .nestedContainer(keyedBy: String.self)
                let restaurant = try place
                    .nestedContainer(keyedBy: String.self, forKey: "place")
                
                // Не мапим через имплементацию Decodable для модели Restaurant
                // потому что линк на картинку не содержит бэйс урл для нее.
                // Что бы не распространять базовый урл в несколько мест приложения,
                // мапим все в интеракторе, оставляя в моделе минимум информации и
                // только возможность управлять размерами картинки
                let name = try restaurant
                    .decode(String.self, forKey: "name")
                let description = try restaurant
                    .decodeIfPresent(String.self, forKey: "description")
                let footerDescription = try restaurant
                    .decodeIfPresent(String.self, forKey: "footerDescription")
                
                let picture = try restaurant
                    .nestedContainer(keyedBy: String.self, forKey: "picture")
                let uri = try picture
                    .decodeIfPresent(String.self, forKey: "uri")
                
                let item = Restaurant(
                    title: name,
                    description: description,
                    footerDescription: footerDescription,
                    imageLinkPattern: uri.map { baseURL + $0 }
                )
                places.append(item)
            }
            self.restaurants = places
        }
    }
}
