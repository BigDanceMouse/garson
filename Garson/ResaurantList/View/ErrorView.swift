//
//  ErrorView.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    
    var onRetryTap: (() -> Void)?
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBAction func retryButtonDidTouched(_ sender: UIButton) {
        self.onRetryTap?()
    }
    
    static func loadFromNib() -> ErrorView {
        let nib = UINib.init(nibName: "ErrorView", bundle: nil)
        let nibObjects = nib.instantiate(withOwner: nil, options: nil)
        return nibObjects.first! as! ErrorView
    }
}
