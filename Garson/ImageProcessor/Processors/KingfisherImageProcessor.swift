//
//  KingfisherImageProcessor.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit
import Kingfisher

struct KingfisherImageProcessor: ImageProcessorProtocol {
    
    func loadImage(from link: String, into imageView: UIImageView) {
        imageView.kf.cancelDownloadTask()
        let url = URL(string: link)
        imageView.kf.setImage(with: url)
    }
    
    func clearCache() {
        KingfisherManager.shared.cache.clearDiskCache()
    }
}
