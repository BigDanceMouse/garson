//
//  ImageLoadProcessor+SettingsPresenter.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation

extension ImageLoadProcessor: SettingsPresenter {
    
    var itemsCount: Int {
        return LoaderType.allCases.count
    }
    
    func item(at index: Int) -> (processorTitle: String, isCurrentlyUsing: Bool) {
        let loaderType = LoaderType.allCases[index]
        let isCurrentlyUsing = self.currentLoader == loaderType
        return (loaderType.rawValue, isCurrentlyUsing)
    }
    
    func didSelectItem(at index: Int) {
        self.currentLoader = LoaderType.allCases[index]
    }
}
