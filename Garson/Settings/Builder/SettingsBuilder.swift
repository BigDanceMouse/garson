//
//  SettingsBuilder.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

class SettingsBuilder: NSObject {

    @IBOutlet weak var viewController: SettingsViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewController.presenter = ImageLoadProcessor.shared
    }
}
