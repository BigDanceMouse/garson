//
//  LayerState.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import Foundation


/// Описывает состояние слоя
///
/// - prepare: Слой загружает данные
/// - ready: Данные готовы к отображению
/// - error: При подготовке данных возникла ошбика
enum LayerState {
    case prepare
    case ready
    case error(message: String)
}

extension LayerState: Equatable { }
