//
//  ImageProcessorProtcol.swift
//  Garson
//
//  Created by Владимир Елизаров on 28/10/2018.
//  Copyright © 2018 Владимир Елизаров. All rights reserved.
//

import UIKit

protocol ImageProcessorProtocol {
    func loadImage(from link: String, into imageView: UIImageView)
    func clearCache()
}
